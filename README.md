# Kelompok B04 Sistem Operasi 2022

### Anggota

| Nama                               | NRP        |
|------------------------------------|------------|
| Lia Kharisma Putri                 | 5025201034 |
| Muhammad Dzikri Fakhrizal Syairozi | 5025201201 |
| Mohamad Kholid Bughowi             | 5025201253 |
<br/>

### Soal 1

# Penjelasan soal-shift-sisop-modul-2-e04-2022

## Soal 1
Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu. 

- Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (d).

- Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA

- Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.

- Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.
Contoh : **157_characters_5_Albedo_53880**

- Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

### Jawaban

Untuk menyelesaikan soal (a), digunakan `wget` untuk mendownload file yang diperlukan pada soal. Diperlukan flag `--no-check-certificate` agar bisa mendownload dari google drive. Kemudian menggunakan `unzip` dengan flag `-qq` agar tidak mengeluarkan output lalu menggunakan `rm` untuk meremove file yang tidak diperlukan lagi.

```c
void extractDownload()
{
    char *urls[] = {"https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
                    "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"};

    char *filename[] = {"characters.zip", "weapons.zip"};
    id_t childID;
    int status;

    if ((childID = fork()) == 0)
    {
        execlp("mkdir", "mkdir", "-p", "gacha_gacha", NULL);
    }

    for (int i = 0; i < 2; i++)
    {
        if ((childID = fork()) == 0)
        {
            execlp("wget", "wget", "--no-check-certificate", urls[i], "-O", filename[i], "-q", NULL);
        }

        while ((wait(&status)) > 0)
            ;

        if ((childID = fork()) == 0)
        {
            execlp("unzip", "unzip", "-qq", filename[i], NULL);
        }

        while ((wait(&status)) > 0)
            ;

        if ((childID = fork()) == 0)
        {
            execlp("rm", "rm", "-f", filename[i], NULL);
        }
        while ((wait(&status)) > 0)
            ;
    }
}

```

Untuk menyelesaikan soal selanjutnya, digunakan fungsi pembantu `gacha()` yang memiliki parameter `gacha_counter` sebagai penghitung jumlah gacha yang diincrement 90. Fungsi menggunakan library `time.h` untuk penamaan file. Apabila `gacha_counter` habis dimodulo 90, maka fungsi akan membuat folder untuk total gacha, dan kemudian akan membuat 10 file txt berisi hasil gacha sesuai dengan format. Gacha kemudian dilakukan dengan melakukan __directory listing__ pada folder weapons dan characters sesuai dengan aturan ganjil genap, dan kemudian mengambil sebuah file secara random dengan fungsi `rand()` milik C. Kemudian setelah sebuah file terambil, file tersebut akan diparse menggunakan library `json-c/json.h`. File txt kemudian dinamai dengan waktu file dibuat dengan file dalam satu folder berselang 1 detik. Agar tidak terjadi masalah saat penambahan waktu, dibuat fungsi `formatTime()` untuk memformat waktu. 

```c
void gacha(int gacha_counter)
{
    id_t childID;
    int status;
    if (gacha_counter == 0)
    {
        if ((chdir("gacha_gacha")) < 0)
        {
            exit(EXIT_FAILURE);
        }
    }
    else if (gacha_counter != 0)
    {
        if ((chdir("../")) < 0)
        {
            exit(EXIT_FAILURE);
        }
    }

    int primo_avail;

    time_t currentTime;
    time(&currentTime);
    struct tm *myTime = localtime(&currentTime);

    char dir_name[22] = "total_gacha_", buffer[10];
    snprintf(buffer, 10, "%d", gacha_counter + 90);
    strcat(dir_name, buffer);

    if (gacha_counter % 90 == 0)
    {
        if ((childID = fork()) == 0)
        {
            execlp("mkdir", "mkdir", "-p", dir_name, NULL);
        }

        while ((wait(&status)) > 0)
            ;
    }

    if (gacha_counter % 10 == 0)
    {
        char r_dir_name[30] = "";
        strcat(r_dir_name, dir_name);
        if ((chdir(r_dir_name)) < 0)
        {
            exit(EXIT_FAILURE);
        }
        srand(time(NULL));
        for (int i = 1; i < 10; i++)
        {
            char file_name[25], buffer2[10];
            struct tm *formattedTime = timeFormat(myTime, 1);
            strftime(file_name, sizeof(file_name), "%H:%M:%S", formattedTime);
            strcat(file_name, "_gacha_");
            snprintf(buffer2, 10, "%d", (i * 10) + gacha_counter);
            strcat(strcat(file_name, buffer2), ".txt");

            if ((childID = fork()) == 0)
            {
                execlp("touch", "touch", file_name, NULL);
            }
            while ((wait(&status)) > 0)
                ;

            FILE *fp;
            fp = fopen(file_name, "a");
            for (int j = 1; j <= 10; j++)
            {
                primo_avail = 79000 - 160 * ((j) + (i * 10) + gacha_counter - 10);
                if (primo_avail < 0)
                    break;
                if (j % 2 != 0)
                {
                    char json_path[70] = "../../characters/";
                    strcat(json_path, getFile("../../characters", rand() % (48 - 1) + 1));
                    fprintf(fp, "%d_characters_%s_%d\n", (j) + (i * 10) + gacha_counter - 10, convertToJson(json_path), primo_avail);
                }
                else
                {
                    char json_path[70] = "../../weapons/";
                    strcat(json_path, getFile("../../weapons", rand() % (130 - 1) + 1));
                    fprintf(fp, "%d_weapons_%s_%d\n", (j) + (i * 10) + gacha_counter - 10, convertToJson(json_path), primo_avail);
                }
            }
            fclose(fp);
            if (primo_avail < 0)
            {
                return;
            }
        }
    }
}

```

Untuk menjalankan program daemon, digunakan template daemon pada modul, yang kemudian ditambah program aplikasi. Jalan program dibagi 2, yaitu pada jam 04.44 tanggal 30-03 dan jam 07.44 tanggal 30-03. Pada bagian pertama, program akan menjalankan fungsi `downloadExtract()` dan `gacha()` sampai primogem habis. Pada bagian kedua, program akan melakukan unzip hasil gacha ke dalam file __not_safe_for_wibu__ dan kemudian program akan meremove file yang tidak diperlukan.


### Soal 2

JJaprun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.
- Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.
- Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.
- Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.
- Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto)
- Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending).

### Jawaban
Untuk mengerjakan soal (a) dimana perintah soal adalah unzip file drakor.zip dan dimasukkan kedalam folder shift2/drakor digunakan `mkdir` untuk membuat folder shift2/drakor, setelah itu digunakan `unzip` dengan flag `-qq` agar tidak mengeluarkan output. Perintah selanjutnya adalah menghapus isi yang tidak diperlukan dalam folder drakor. Karena yang diperlukan hanya file bertipe png maka selain png akan dihapus menggunakan `rm`.

```c
void unzipfile()
{
	
	id_t child_id;
	int status;
	
	if((child_id=fork())==0)
	{
		execlp("mkdir","mkdir","-p",path,NULL);
	}
	
	while ((wait(&status))>0)
	;
	
	if((child_id=fork())==0)
	{
		execlp("unzip","unzip","-qq",filename,"-d",path,NULL);
	}
	while ((wait(&status))>0)
	;
}

void rmfile(){
   struct dirent *dp;
   DIR *folder;
   folder = opendir(path);

   if(folder != NULL){ 
       while((dp = readdir(folder)) != NULL){
            if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
                 if(dp->d_type == DT_DIR){
                 
			char pathrm[400];
			sprintf(pathrm, "%s/%s", path, dp->d_name);

			id_t child_id;
			int status;
			
			if((child_id=fork())==0)
			{
				execlp("rm","rm","-r",pathrm,NULL);
			}
			
			while ((wait(&status))>0)
			;
                 }
            }
       }
   }
}

```
Selanjutnya untuk soal (b) digunakan `strtok` untuk memisah nama file dari judul,tahun rilis dan genrenya. setelah itu dibuat folder sesuai nama genre yang didapat dari semua file menggunakan `mkdir`.
Untuk soal (c) diperintahkan untuk memindah poster sesuai dengan genrenya kedalam folder genre yang telah dibuat di soal (b). maka digunakan `cp` untuk mengcopy poster kedalam folder genre masing-masing. Karena didalam satu poster bisa terdapat dua judul sesuai soal (d) maka akan dicopy kedalam foldernya masing-masing. Untuk memisahkan antara judul 1 dan judul 2 digunakan `strtok` dengan pemisah (_). Selanjutnya untuk soal (e) dibuat file .txt untuk menyimpan data poster disetiap foldernya. 

```c
void kategori(char *arrpath, char *filename)
{
	id_t child_id;
	int status;
	char *judul = strtok(filename, ";");
	char *tahun = strtok(NULL, ";");
	char *genre = strtok(NULL, ";");
	char *genrenopng = strtok(genre, ".");
	
	char pathkategori[250],pathfilename[250],pathtxt[250];
	
	sprintf(pathkategori, "%s/%s", path, genrenopng);
	sprintf(pathfilename, "%s/%s/%s.png", path, genre,judul);
	sprintf(pathtxt, "%s/%s/data.txt", path, genre);
	
	
	if((child_id=fork())==0)
	{
		execlp("mkdir","mkdir","-p",pathkategori,NULL);
	}
	
	while ((wait(&status))>0)
	;
	
	if((child_id=fork())==0)
	{
		execlp("cp","cp",arrpath,pathfilename,NULL);
	}
	
	while ((wait(&status))>0)
	;
	
	char kategoritxt[300];
	char isitxt[300];
	sprintf(isitxt, "kategori : %s \nnama : %s\nrilis : tahun %s\n\n", genre, judul,tahun);
	
	FILE *filetxt;
	filetxt = fopen(pathtxt, "a");
	
	if(filetxt) {
        fprintf(filetxt, "%s", isitxt);
        fclose(filetxt);
    }
}

int main () {
  	
	unzipfile();
	rmfile();
  	
    char temp[400];
    
    strcpy(temp, path);
    strcat(temp, "/");	
    struct dirent *dp;
    DIR *folder;
    folder = opendir(temp);

    if(folder != NULL){ 
       while((dp = readdir(folder)) != NULL){
            if(strcmp(dp->d_name, ".")&& strcmp(dp->d_name, "..") ){
                 if(dp->d_type == DT_REG){
                      char arrpath[400], filename[400];
                      sprintf(arrpath, "%s/%s",path, dp->d_name);
                      sprintf(filename, "%s",  dp->d_name);
                      

                      char *drakor1 = strtok(filename, "_");
                      char *drakor2 = strtok(NULL, "_");
                      if(drakor2 != NULL){                      
                          kategori(arrpath, drakor2);
                      }
                      kategori(arrpath,drakor1);

                      pid_t pid;
                      pid = fork();
                      if(pid == 0){
                         char *argv[] = {"rm", "-rf", arrpath, NULL};
                         execv("/usr/bin/rm", argv);  
                      }
                      while(wait(NULL) != pid);
                 }
            }
            
       }
   }

  }

```

### Soal 3

Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang.

#### Poin A

Pada poin A diminta untuk membuat folder `modul2/darat` dan `modul2/air`. Folder `modul2/air` dibuat setelah 3 detik folder `modul2/darat` selesai terbuat.
Berikut adalah syntax c untuk membuat folder `modul2` dan folder `darat` di dalam modul2.

```c
pid_t child;
int status;
child = fork();
if (child < 0)
{
    exit(EXIT_FAILURE);
}
if (child == 0)
{
    execl("/bin/mkdir", "mkdir", "-p", "modul2", NULL);
}
while ((wait(&status)) > 0);

child = fork();
if (child < 0)
{
    exit(EXIT_FAILURE);
}
if (child == 0)
{
    execl("/bin/mkdir", "mkdir", "-p", "modul2/darat", NULL);
}
while ((wait(&status)) > 0);
```

Setelah 3 detik, dibuat folder `modul2/air`. Disini digunakan `sleep(3)` untuk menunda program selanjutnya dieksekusi selama 3 detik.

```c
child = fork();
if (child < 0)
{
    exit(EXIT_FAILURE);
}
if (child == 0)
{
    sleep(3);
    execl("/bin/mkdir", "mkdir", "-p", "modul2/air", NULL);
}
while ((wait(&status)) > 0);
```

#### Poin B

Selanjutnya dilakukan ekstrak file `animal.zip` kedalam folder `modul2`.

```c
child = fork();
if (child < 0)
{
    exit(EXIT_FAILURE);
}
if (child == 0)
{
    execl("/bin/unzip", "unzip", "animal.zip", "-d", "modul2", NULL);
}
while ((wait(&status)) > 0);
```

#### Poin C

Memindahkan nama-nama hewan kedalam folder darat dan air. Digunakan dirent untuk membaca tiap file dalam folder `modul2`. Lalu dari nama file yang terbaca, cek apakah terdapat string `"_darat"` atau `"_air"`. Jika terdapat substring `"_darat"` maka masukkan file tersebut ke folder darat. Jika substringnya `"_air"` maka masukkan ke folder air. Untuk memasukkanya ke masing-masing folder menggunakan perintah `cp` pada linux.

```c
DIR *d;
struct dirent *dir;
d = opendir("modul2/animal");
while ((dir = readdir(d)) != NULL)
{
    if (strstr(dir->d_name, "_darat") != NULL)
    {
        child = fork();
        if (child < 0)
        {
            exit(EXIT_FAILURE);
        }
        if (child == 0)
        {
            char *path = (char *)malloc(100);
            sprintf(path, "modul2/animal/%s", dir->d_name);
            execl("/bin/cp", "cp", path, "modul2/darat", NULL);
        }
        while ((wait(&status)) > 0)
            ;
    }
    else if (strstr(dir->d_name, "_air") != NULL)
    {
        child = fork();
        if (child < 0)
        {
            exit(EXIT_FAILURE);
        }
        if (child == 0)
        {
            char *path = (char *)malloc(100);
            sprintf(path, "modul2/animal/%s", dir->d_name);
            execl("/bin/cp", "cp", path, "modul2/air", NULL);
        }
        while ((wait(&status)) > 0)
            ;
    }
}
closedir(d);
```

#### Poin D

Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air, diminta menghapus nama-nama file di dalam folder darat yang mengandung substring `"bird"`. Caranya hampir sama seperti di poin c. Dengan dirent, kita baca semua file di folder darat. Apabila nama file yang terbaca terdapat substring `"bird"`, maka file itu dihapus dengan fungsi `remove()` di C.

```c
DIR *d2;
struct dirent *darat;
d2 = opendir("modul2/darat");
while ((darat = readdir(d2)) != NULL)
{
    if (strstr(darat->d_name, "bird") != NULL)
    {
        char *path = (char *)malloc(200);
        sprintf(path, "modul2/darat/%s", darat->d_name);
        remove(path);
    }
}
closedir(d2);
```

#### Poin E

Di folder air, dibuat file list.txt yang mendata nama-nama file apa saja yang ada dalam folder tersebut beserta permissions dan user yang sedang mengaksesnya. Dengan cara yang hampir sama seperti poin D, dibaca satu persatu nama file yang ada di folder air. Untuk memastikan agar file list.txt dan direktori "." atau ".." tidak ikut diproses dengan syntax `if (strstr(air->d_name, ".txt") == NULL && air->d_type != DT_DIR)`. Digunakan fungsi `fopen()` untuk membuat file `list.txt` dengan mode `"a"` agar file tidak dibuat ulang setiap kali kita menulis. Kemudian untuk mendapatkan user yang sedang login, dipakai fungsi `getlogin()`, mendapatkan permission file digunakan fungsi `permissions()`.

```c
DIR *d3;
struct dirent *air;
d3 = opendir("modul2/air");
while ((air = readdir(d3)) != NULL)
{
    FILE *write_file = fopen("modul2/air/list.txt", "a");
    if (strstr(air->d_name, ".txt") == NULL && air->d_type != DT_DIR)
    {
        char *path = (char *)malloc(100);
        sprintf(path, "modul2/air/%s", air->d_name);
        fprintf(write_file, "%s_%s_%s\n", getlogin(), permissions(path), air->d_name);
    }
    fclose(write_file);
}
closedir(d3);
```

Berikut syntax fungsi `char *permissions(char *file)`. Fungsi ini menerima argumen `char *file`, yakni path menuju file. Dan mengembalikan permissions dalam bentuk `char *` apabila berhasil, dan `strerror(errno)` apabila gagal. Contoh return dari fungsi ini apabila berhasil adalah `rwxrwxrwx`.

```c
char *permissions(char *file)
{
    struct stat st;
    char *modeval = malloc(sizeof(char) * 9 + 1);
    if (stat(file, &st) == 0)
    {
        mode_t perm = st.st_mode;
        modeval[0] = (perm & S_IRUSR) ? 'r' : '-';
        modeval[1] = (perm & S_IRUSR) ? 'w' : '-';
        modeval[2] = (perm & S_IRUSR) ? 'x' : '-';
        modeval[3] = (perm & S_IRUSR) ? 'r' : '-';
        modeval[4] = (perm & S_IRUSR) ? 'w' : '-';
        modeval[5] = (perm & S_IRUSR) ? 'x' : '-';
        modeval[6] = (perm & S_IRUSR) ? 'r' : '-';
        modeval[7] = (perm & S_IRUSR) ? 'w' : '-';
        modeval[8] = (perm & S_IRUSR) ? 'x' : '-';
        modeval[9] = '\0';
        return modeval;
    }
    else
    {
        return strerror(errno);
    }
}
```
