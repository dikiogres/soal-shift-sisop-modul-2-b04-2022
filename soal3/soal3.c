#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>

char *permissions(char *file)
{
    struct stat st;
    char *modeval = malloc(sizeof(char) * 9 + 1);
    if (stat(file, &st) == 0)
    {
        mode_t perm = st.st_mode;
        modeval[0] = (perm & S_IRUSR) ? 'r' : '-';
        modeval[1] = (perm & S_IRUSR) ? 'w' : '-';
        modeval[2] = (perm & S_IRUSR) ? 'x' : '-';
        modeval[3] = (perm & S_IRUSR) ? 'r' : '-';
        modeval[4] = (perm & S_IRUSR) ? 'w' : '-';
        modeval[5] = (perm & S_IRUSR) ? 'x' : '-';
        modeval[6] = (perm & S_IRUSR) ? 'r' : '-';
        modeval[7] = (perm & S_IRUSR) ? 'w' : '-';
        modeval[8] = (perm & S_IRUSR) ? 'x' : '-';
        modeval[9] = '\0';
        return modeval;
    }
    else
    {
        return strerror(errno);
    }
}

int main()
{
    pid_t child;
    int status;
    child = fork();
    if (child < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child == 0)
    {
        execl("/bin/mkdir", "mkdir", "-p", "modul2", NULL);
    }
    while ((wait(&status)) > 0)
        ;

    child = fork();
    if (child < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child == 0)
    {
        execl("/bin/mkdir", "mkdir", "-p", "modul2/darat", NULL);
    }
    while ((wait(&status)) > 0)
        ;

    child = fork();
    if (child < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child == 0)
    {
        sleep(3);
        execl("/bin/mkdir", "mkdir", "-p", "modul2/air", NULL);
    }
    while ((wait(&status)) > 0)
        ;

    child = fork();
    if (child < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child == 0)
    {
        execl("/bin/unzip", "unzip", "animal.zip", "-d", "modul2", NULL);
    }
    while ((wait(&status)) > 0)
        ;

    DIR *d;
    struct dirent *dir;
    d = opendir("modul2/animal");
    while ((dir = readdir(d)) != NULL)
    {
        if (strstr(dir->d_name, "_darat") != NULL)
        {
            child = fork();
            if (child < 0)
            {
                exit(EXIT_FAILURE);
            }
            if (child == 0)
            {
                char *path = (char *)malloc(100);
                sprintf(path, "modul2/animal/%s", dir->d_name);
                execl("/bin/cp", "cp", path, "modul2/darat", NULL);
            }
            while ((wait(&status)) > 0)
                ;
        }
        else if (strstr(dir->d_name, "_air") != NULL)
        {
            child = fork();
            if (child < 0)
            {
                exit(EXIT_FAILURE);
            }
            if (child == 0)
            {
                char *path = (char *)malloc(100);
                sprintf(path, "modul2/animal/%s", dir->d_name);
                execl("/bin/cp", "cp", path, "modul2/air", NULL);
            }
            while ((wait(&status)) > 0)
                ;
        }
    }
    closedir(d);

    DIR *d2;
    struct dirent *darat;
    d2 = opendir("modul2/darat");
    while ((darat = readdir(d2)) != NULL)
    {
        printf("%s\n", darat->d_name);
        if (strstr(darat->d_name, "bird") != NULL)
        {
            char *path = (char *)malloc(200);
            sprintf(path, "modul2/darat/%s", darat->d_name);
            remove(path);
        }
    }
    closedir(d2);

    DIR *d3;
    struct dirent *air;
    d3 = opendir("modul2/air");
    while ((air = readdir(d3)) != NULL)
    {
        FILE *write_file = fopen("modul2/air/list.txt", "a");
        if (strstr(air->d_name, ".txt") == NULL && air->d_type != DT_DIR)
        {
            char *path = (char *)malloc(100);
            sprintf(path, "modul2/air/%s", air->d_name);
            fprintf(write_file, "%s_%s_%s\n", getlogin(), permissions(path), air->d_name);
        }
        fclose(write_file);
    }
    closedir(d3);

    return 0;
}
